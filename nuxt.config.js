export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'qfit_admin',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'},
    ],
    link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.css',
    '~/assets/css/montserrat_family.css',
    '~/assets/css/index.css',
    '~/assets/css/login.css',
    '~/assets/css/settings.css',
    '~/assets/css/history.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~plugins/vue-js-modal.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',

    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/apollo',
  ],
  moment: {
    timezone: true,
    defaultTimezone: 'UTC/GMT',
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://qfit-back-2-qg7ev.ondigitalocean.app/', // Used as fallback if no runtime config is provided
    // baseURL: 'https://d9a6-89-219-8-58.ngrok.io',
  },
  // Apollo configuration
  apollo: {
    clientConfigs: {
      default: {
        // httpEndpoint: 'https://d9a6-89-219-8-58.ngrok.io/graphql',
        httpEndpoint: 'https://qfit-back-2-qg7ev.ondigitalocean.app/graphql',
      },
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map';
      }
    },
  },
};
