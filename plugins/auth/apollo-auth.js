import clubQuery from '~/graphql/queries/clubByID.js';
import loginQuery from '~/graphql/queries/login.js';

export default class ApolloScheme {
  clubId = null;

  constructor(auth, options) {
    this.$auth = auth;
    this.$apollo = auth.ctx.app.apolloProvider.defaultClient;
    this.$apolloHelpers = auth.ctx.app.$apolloHelpers;
    this._name = options._name;
  }

  async login({data, tokenKey = 'token', userKey = 'user'}) {
    return this.$apollo
      .query({
        query: loginQuery,
        variables: data,
      })
      .then(({data}) => Promise.resolve(Object.values(data)))
      .then(data => {
        this.clubId = data.ownedClubID;
        this.setUserToken(data.token);
      });
  }

  setToken(tokenValue) {
    return this.$apolloHelpers
      .onLogin(tokenValue)
      .then(() => this.$auth.setToken(this._name, tokenValue));
  }

  setUser(user) {
    return this.$auth.setUser(user);
  }

  setUserToken(tokenValue) {
    return this.setToken(tokenValue).then(() => this.fetchUser());
  }

  check() {
    const status =
      !!this.$auth.getToken(this._name) &&
      this.$auth.getToken(this._name) !== this._name;
    return status;
  }

  fetchUser() {
    if (!this.check()) {
      return Promise.reject(new Error('Can not check user'));
    }

    return this.$apollo
      .query({query: clubQuery, variables: {id: this.clubId}})
      .then(({data}) => Promise.resolve(Object.values(data)))
      .then(data => this.setUser(data));
  }

  logout() {
    return this.$apolloHelpers.onLogout().then(() => this.reset());
  }

  reset() {
    return this.setToken(this._name, null).then(() => this.setUser(null));
  }
}
