module.exports = {
  // purge: [
  //   './components/**/*.{vue,js}',
  //   './layouts/**/*.vue',
  //   './pages/**/*.vue',
  //   './plugins/**/*.{js,ts}',
  //   './nuxt.config.{js,ts}',
  // ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Montserrat Regular', 'system-ui'],
    },

    extend: {
      colors: {
        primary: '#7E3EEA',
        white: '#FCFCFC',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
