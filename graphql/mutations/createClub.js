import {gql} from 'graphql-tag';

export const createClubMutation = gql`
  mutation (
    $_id: String
    $address: String!
    $averagePrice: Float!
    $city: String!
    $contact: ContactInput!
    $coordinates: CordinatesInp!
    $description: String!
    $gallery: [Upload!]!
    $name: String!
    $photo: Upload!
  ) {
    createClubAdmin(
      _id: $_id
      address: $address
      averagePrice: $averagePrice
      city: $city
      contact: $contact
      cordinates: $coordinates
      description: $description
      gallery: $gallery
      name: $name
      photo: $photo
    ) {
      city
    }
  }
`;
