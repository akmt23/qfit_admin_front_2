import {gql} from 'graphql-tag';

const startSessionOwner = gql`
  mutation ( $userId: String!, $duration: Float!, $serviceId: String!) {
    startSessionOwner( userId: $userId, duration: $duration, serviceId: $serviceId) {
      _id
    }
  }
`;
export default startSessionOwner;
