export const editClubAdmin = `
  mutation (
    $_id: String!
    $address: String
    $averagePrice: Float
    $city: String
    $contact: ContactInput
    $cordinates: CordinatesInp
    $description: String
    $gallery: [Upload!]
    $name: String
    $photo: Upload
  ) {
    editClubAdminOwner(
      _id: $_id
      address: $address
      averagePrice: $averagePrice
      city: $city
      contact: $contact
      cordinates: $cordinates
      description: $description
      gallery: $gallery
      name: $name
      photo: $photo
    ) {
      name
    }
  }
`;
