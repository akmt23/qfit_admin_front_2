import {gql} from 'graphql-tag';

const finishTrainingOwner = gql`
  mutation ($userId: String!) {
    finishTrainingOwner(userId: $userId) {
      userId
    }
  }
`;

export default finishTrainingOwner;
