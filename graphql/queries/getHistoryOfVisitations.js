import {gql} from 'graphql-tag';

const getHistoryOfVisitations = gql`
  query ($page: Int!) {
    getHistoryOfVisitations(page: $page) {
      _id
       generalPrice
      startTime
      endTime
      user {
        _id
        fullName
        phoneNumber
      }
    }
  }
`;

export default getHistoryOfVisitations;
