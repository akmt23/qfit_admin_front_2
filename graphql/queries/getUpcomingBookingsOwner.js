import {gql} from 'graphql-tag';

const getUpcomingBookingsOwner = gql`
  query ($page: Int!) {
    getUpcomingBookingsOwner(page: $page) {
      _id

      service {
        _id
        name
        startTime
        endTime
      }
      user {
        _id
        phoneNumber
        fullName
      }
    }
  }
`;

export default getUpcomingBookingsOwner;
