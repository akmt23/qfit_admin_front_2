import gql from 'graphql-tag';

const GET_CLUB_ADMIN_OWNER = gql`
  query {
    getClubAdminOwner {
      address
      averagePrice
      categories
      city
      contact {
        phoneNumber
        email
      }
      cordinates {
        lat
        lng
      }
      dateAdded
      description
      galleryURLs {
        M
        XL
        thumbnail
      }
      name
      photoURL {
        M
        XL
        thumbnail
      }
      ratingCount
      ratingSum
    }
  }
`;

export default GET_CLUB_ADMIN_OWNER;
