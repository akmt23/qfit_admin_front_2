import {gql} from 'graphql-tag';

const getCurrentSessionsOwner = gql`
  query ($page: Int!) {
    getCurrentSessionsOwner(page: $page) {
      _id
      stack {
        service {
          _id
          name
          startTime
          endTime
        }
      }
      user{
        _id
        phoneNumber
      }
    }
  }
`;

export default getCurrentSessionsOwner;
