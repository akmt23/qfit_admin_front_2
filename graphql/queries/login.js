import {gql} from 'graphql-tag';

const LOGIN_QUERY = gql`
  query ($email: String!, $password: String!) {
    loginOwner(email: $email, password: $password) {
      token
      ownedClubID
      fullName
    }
  }
`;

export default LOGIN_QUERY;
